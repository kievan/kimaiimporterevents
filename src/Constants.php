<?php
/**
 *  Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license.
 *  See LICENSE in the project root for license information.
 *
 *  PHP version 5
 *
 *  @category Code_Sample
 *  @package  php-connect-rest-sample
 *  @author   Ricardo Loo <ricardol@microsoft.com>
 *  @license  MIT License
 *  @link     http://github.com/microsoftgraph/php-connect-rest-sample
 */

namespace Microsoft\Graph\Connect;

/**
 *  Stores constant and configuration values used through the app
 *
 *  @class    Constants
 *  @category Code_Sample
 *  @package  php-connect-rest-sample
 *  @author   Ricardo Loo <ricardol@microsoft.com>
 *  @license  MIT License
 *  @link     http://github.com/microsoftgraph/php-connect-rest-sample
 */
interface Constants
{
    const REDIRECT_URI = 'http://{hostName}/oauth.php';
    const AUTHORITY_URL = 'https://login.microsoftonline.com/common';
    const AUTHORIZE_ENDPOINT = '/oauth2/v2.0/authorize';
    const TOKEN_ENDPOINT = '/oauth2/v2.0/token';
    const RESOURCE_ID = 'https://outlook.office.com/api';
    const SIGNED_IN_USER_INFO = '/v2.0/me';
    const SENDMAIL_ENDPOINT = '/v2.0/me/sendmail';
    const CALENDAR_ENDPOINT = '/v2.0/me/calendarview?startDateTime={startDateTime}&endDateTime={endDateTime}&$select=Subject,Body,Start,End,ShowAs';
    const TIMEZONE = 'Europe/Kiev';
    const SCOPES = 'https://outlook.office.com/Calendars.Read';
}
