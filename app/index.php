<?php
/**
 *  Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license.
 *  See LICENSE in the project root for license information.
 *
 *  PHP version 5
 *
 *  @category Code_Sample
 *  @package  php-connect-rest-sample
 *  @author   Ricardo Loo <ricardol@microsoft.com>
 *  @license  MIT License
 *  @link     http://github.com/microsoftgraph/php-connect-rest-sample
 */
 
/*! 
    @abstract This is the start page. It should display minimal UI emphasizing 
              the "connect" button.
 */

require_once __DIR__ . '/../vendor/autoload.php';

use Microsoft\Graph\Connect\Constants;

// User clicked the "connect" button. Start the authentication flow.
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    header('Location: /oauth.php');
    exit();
} else {
    // Destroy the session. Alternatively, we can remove the individual variables.
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    session_destroy();
}

?>
      
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Kimai Importer Events</title>

  <!-- Third party dependencies. -->
  <link 
      rel="stylesheet" 
      href="https://appsforoffice.microsoft.com/fabric/1.0/fabric.css">
  <link 
      rel="stylesheet" 
      href="https://appsforoffice.microsoft.com/fabric/1.0/fabric.components.css">
  
  <!-- App code. -->
  <link rel="stylesheet" href="./css/styles.css">

  <link rel="shortcut icon" href="favicon.ico">
  
</head>

<body class="ms-Grid">
    <form action="" method="post">
    <div class="ms-Grid-row">
    <!-- App navigation bar markup. -->
        <div class="ms-NavBar">
            <ul class="ms-NavBar-items">
                <li class="navbar-header">Kimai Importer Events</li>
            </ul>
        </div>

    <!-- App main content markup. -->
    <div class="ms-Grid-col ms-u-mdPush1 ms-u-md9 ms-u-lgPush1 ms-u-lg6">
        <div>
            <p class="ms-font-xl">Use the button below to connect to Kimai Importer Events.</p>
            <button class="ms-Button">
                <span class="ms-Button-label">Connect to Kimai Importer Events</span>
            </button>
        </div>
    </div>
    </div>
    </form>
</body>

</html>
