$(document).ready(function(){
    var fromDateInput = $('input[name="fromDate"]'); //our date input has the name "fromDate"
    var toDateInput = $('input[name="toDate"]'); //our date input has the name "toDate"
    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    var options={
        format: 'yyyy/mm/dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
        weekStart: 1
    };
    fromDateInput.datepicker(options);
    toDateInput.datepicker(options);
});