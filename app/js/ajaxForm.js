$(function() {
    // Get the form.
    var form = $('#dateForm');

    // Get the messages div.
    var formMessages = $('#form-messages');
    formMessages.css("padding", "5px");

    $(form).submit(function(event) {
        // Stop the browser from submitting the form.
        event.preventDefault();

        var formData = $(form).serialize();
        formMessages.text('Filling out time tracker (kimai)...');
        formMessages.css("background-color", "#fff");
        $.ajax({
            type: 'POST',
            url: $(form).attr('action'),
            data: formData
        }).done(function(response) {
            console.log(response);
            var message = '',
                jsonResponse = {};
            try {
                jsonResponse = JSON.parse(response);
            } catch(e) {
                message = "Error parsing response.";
                console.log();
            }
            if(message){
                formMessages.text(message);
            } else if (typeof jsonResponse.success !== "undefined" && true === jsonResponse.success) {
                formMessages.text('Done!');
                formMessages.css("background-color", "#11aa22");
            } else if (typeof jsonResponse.success !== "undefined" && false === jsonResponse.success){

                if(typeof jsonResponse.message !== "undefined" && jsonResponse.message){
                    message = jsonResponse.message;
                } else {
                    message = 'Oops something went wrong.';
                }

                formMessages.text(message);
                formMessages.css("background-color", "#aa1122");

            } else {
                console.log('🎝 never 🎝 ever ♩ gonna ♪ give ♫ you ♬ up 🎜');
            }
        }).fail(function(response) {
            console.log("Failed: ", response);
            formMessages.text('Oops something went wrong.');
            formMessages.css("background-color", "#aa1122");
        });
    });
});