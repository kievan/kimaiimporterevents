<?php
/**
 *  Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license.
 *  See LICENSE in the project root for license information.
 *
 *  PHP version 5
 *
 *  @category Code_Sample
 *  @package  php-connect-rest-sample
 *  @author   Ricardo Loo <ricardol@microsoft.com>
 *  @license  MIT License
 *  @link     http://github.com/microsoftgraph/php-connect-rest-sample
 */
 
/*! 
    @abstract The page that the user will be redirected to after 
              Azure Active Directory (AD) finishes the authentication flow.
 */

require_once __DIR__ . '/../vendor/autoload.php';

$kimaiUserName = (getenv('kimaiUserName')) ? : "default";
$configFilePath = __DIR__ . '/kimaiimporter/config.' . $kimaiUserName . '.php';
try {
    if(!file_exists($configFilePath)){
        throw new \Exception("$configFilePath does not exist, please create it by copying config.default.php.example");
    }
} catch(\Throwable $t) {
    echo PHP_EOL . $t->getMessage() . PHP_EOL;
    exit;
}
require_once($configFilePath);

use Microsoft\Graph\Connect\Constants;

//We store user name, id, and tokens in session variables
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$hostName = $_SERVER['HTTP_HOST'] ?  : 'localhost:8000';
$redirectUri = preg_replace("/{hostName}/", $hostName, Constants::REDIRECT_URI);

$provider = new \League\OAuth2\Client\Provider\GenericProvider([
    'clientId'                => $config['outlookOfficeApi']['clientId'],
    'clientSecret'            => $config['outlookOfficeApi']['clientSecret'],
    'redirectUri'             => $redirectUri,
    'urlAuthorize'            => Constants::AUTHORITY_URL . Constants::AUTHORIZE_ENDPOINT,
    'urlAccessToken'          => Constants::AUTHORITY_URL . Constants::TOKEN_ENDPOINT,
    'urlResourceOwnerDetails' => '',
    'scopes'                  => Constants::SCOPES,
    'state'                   => hash('sha256', openssl_random_pseudo_bytes(32))
]);

if ($_SERVER['REQUEST_METHOD'] === 'GET' && !isset($_GET['code']) && !isset($_GET['error'])) {
    $authorizationUrl = $provider->getAuthorizationUrl();

    // The OAuth library automaticaly generates a state value that we can
    // validate later. We just save it for now.
    $_SESSION['state'] = $provider->getState();

    header('Location: ' . $authorizationUrl);
    exit();
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['error'])) {
    // Answer from the authentication service contains an error.
    printf('Something went wrong while authenticating: [%s] %s', $_GET['error'], $_GET['error_description']);
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['code'])) {
    // Validate the OAuth state parameter
    if (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['state'])) {
        unset($_SESSION['state']);
        header('Location: /disconnect.php');
    }

    // With the authorization code, we can retrieve access tokens and other data.
    try {
        // Get an access token using the authorization code grant
        $accessToken = @$provider->getAccessToken('authorization_code', [
            'code'     => $_GET['code']
        ]);
        $_SESSION['access_token'] = $accessToken->getToken();
        
        // The id token is a JWT token that contains information about the user
        // It's a base64 coded string that has a header, payload and signature
        $idToken = $_SESSION['access_token'];
        $decodedAccessTokenPayload = base64_decode(
            explode('.', $idToken)[1]
        );
        $jsonAccessTokenPayload = json_decode($decodedAccessTokenPayload, true);

        // The following user properties are needed in the next page
//        $_SESSION['preferred_username'] = $jsonAccessTokenPayload['preferred_username'];
        $_SESSION['given_name'] = $jsonAccessTokenPayload['name'];

        header('Location: calendarview.php');
        exit();
    } catch (League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
        printf('Something went wrong, couldn\'t get tokens: %s', $e->getMessage());
    }
}
