<?php

require_once("../autoload.php");

use Microsoft\Graph\Connect\RequestManager;
use Microsoft\Graph\Connect\Constants;

//We store user name, id, and tokens in session variables
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    try {

        $meUrl = Constants::RESOURCE_ID . Constants::SIGNED_IN_USER_INFO;
        $response = RequestManager::sendRequest(
            $meUrl,
            [
                "Authorization: Bearer {$_SESSION['access_token']}",
                'Prefer: outlook.body-content-type="text"'
            ]
        );
        $response = json_decode($response, true);


        $renderedTemplate = preg_replace(
            ["/{userFullName}/", "/{userEmail}/",],
            [$response["DisplayName"], $response["EmailAddress"],],
            file_get_contents('./templates/calendarview.html')
        );

        echo $renderedTemplate;

    } catch (\RuntimeException $e) {
        echo $e->getMessage();
    }
} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    try {
        $tz = new \DateTimeZone(Constants::TIMEZONE);
        $startDateTime = (new \DateTime($_POST['fromDate']))
            ->setTimezone($tz)
            ->modify('midnight')
            ->modify('1 second')
            ->format('c');
        $endDateTime = (new \DateTime($_POST['toDate']))
            ->setTimezone($tz)
            ->modify('tomorrow')
            ->modify('1 second ago')
            ->format('c');

        $calendarViewUrl = Constants::RESOURCE_ID
            . preg_replace(
                ["/{startDateTime}/", "/{endDateTime}/"],
                [urlencode($startDateTime), urlencode($endDateTime)],
                Constants::CALENDAR_ENDPOINT);

        $accessToken = $_SESSION['access_token'] ?? 'no_access_token';
        $calendarEvents = RequestManager::sendRequest(
            $calendarViewUrl,
            [
                "Authorization: Bearer $accessToken",
                'Prefer: outlook.body-content-type="text"',
                'Prefer: outlook.timezone="' . Constants::TIMEZONE . '"'
            ]
        );
        $response = ["success" => true];
        $calendarEvents = json_encode(json_decode($calendarEvents, true), JSON_PRETTY_PRINT);
        file_put_contents(__DIR__ . '/kimaiimporter/office365Events.json', $calendarEvents);
        $command = "php kimaiimporter/importToKimai.php --s {$_POST['fromDate']} --e {$_POST['toDate']}";
        $response["result"] = exec($command);
    } catch (\Throwable $t) {
        $response = ["success" => false, "message" => $t->getMessage()];
    }

    echo json_encode($response);
}

