<?php

class Kimai
{

    private $username;
    private $password;

    private $baseUrl;
    private $loginUrl;
    private $mainUrl;
    private $addEntryUrl;

    private $kimaiKey;
    private $kimaiUserId;

    private $requestHeaders = [];
    private $postFields = [];

    const DEFAULT_PROJECT_NAME = "6S_Others (6-Systems)()";
    const DEFAULT_ACTIVITY_NAME = "Project_work";


    /**
     * Kimai constructor.
     *
     * @param array $params
     */
    function __construct($params)
    {

        $endpoints = $params['endpoints'];
        $this->baseUrl = $endpoints['base'];
        $this->loginUrl = $this->baseUrl . $endpoints['login'];
        $this->mainUrl = $this->baseUrl . $endpoints['main'];
        $this->addEntryUrl = $this->baseUrl . $endpoints['add'];

        $this->username = $params['username'];
        $this->password = $params['password'];

        $this->login();
        $this->getAndSetUserId();
    }


    public function addEntry($params)
    {
        $this->_setRequestHeaders();

        $kimaiProject = empty($params['kimai_project']) ? self::DEFAULT_PROJECT_NAME : $params['kimai_project'];
        $kimaiActivity = empty($params['kimai_activity']) ? self::DEFAULT_ACTIVITY_NAME : $params['kimai_activity'];

        $projectId = 0;
        $activityId = 0;
        if(!is_numeric($kimaiProject) && !is_numeric($kimaiActivity)) {
            $projectAndActivityIds = $this->getProjectAndActivityIds($kimaiProject, $kimaiActivity);
            $projectId = $projectAndActivityIds[$kimaiProject];
            $activityId = $projectAndActivityIds[$kimaiActivity];
        } else {
            $projectId = $kimaiProject;
            $activityId = $kimaiActivity;
            if($projectId === 153){
		        $activityId = 145; // General Configuration
            }
        }

        $postFields = [

            // Come from event
            "description" => $params['description'],
            "start_day" => $params['start_day'],
            "end_day" => $params['end_day'],
            "start_time" => $params['start_time'],
            "end_time" => $params['end_time'],
            "duration" => $params['duration'],

            // Generated internally
            "projectID" => $projectId,
            "activityID" => $activityId,
            "userID[]" => $this->kimaiUserId,

            // Always present
            "id" => '',
            "axAction" => 'add_edit_timeSheetEntry',
            "commentType" => 0,
            "statusID" => 1,
            "billable" => 0,
            'filter' => '',
            'location' => '',
            'trackingNumber' => '',
            'comment' => '',
            'budget' => '',
            'approved' => '',
        ];
        $this->_setPostFields($postFields);
        $url = $this->addEntryUrl;
        $result = $this->getData($url);

        return $result;
    }


    public function getKimaiKey()
    {

        return $this->kimaiKey;
    }


    public function getKimaiUserId()
    {

        return $this->kimaiUserId;
    }


    /**
     * Get data from Kimai server
     *
     * @param string  $url
     * @param boolean $fromHeader
     *
     * @return mixed
     */
    protected function getData($url, $fromHeader = false)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if ($fromHeader) {
            curl_setopt($ch, CURLOPT_HEADERFUNCTION, [$this, "setKimaiKey"]);
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->requestHeaders);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->postFields));

        $result = curl_exec($ch);

        return $result;
    }


    private function setKimaiKey($curl, $headerLine)
    {

        /**
         * https://github.com/kimai/kimai/blob/aa4a21d2cced8a90e0764969bc63c7150c8c76cb/includes/func.php#L191
         */
        preg_match("/kimai_key=([a-zA-Z0-9]*)/", $headerLine, $m);
        if (!empty($m[1])) {
            $this->kimaiKey = $m[1];
        }

        return strlen($headerLine);
    }


    private function login()
    {
        $this->_setRequestHeaders(true);

        $postFields = [
            'a' => 'checklogin',
            'name' => $this->username,
            'password' => $this->password
        ];
        $this->_setPostFields($postFields);

        $url = $this->loginUrl;
        $this->getData($url, true);
    }


    private function getAndSetUserId()
    {
        $this->_setRequestHeaders();
        $this->_setPostFields([]);
        $url = $this->mainUrl;
        $result = $this->getData($url);

        preg_match("/userID[\ ]+=\ (\d+)/", $result, $match);

        $this->kimaiUserId = $match[1];
    }


    private function getProjects()
    {

    }


    public function getProjectAndActivityIds($kimaiProject, $kimaiActivity)
    {
        $map_project_name_to_project_id = [
            'Scrum_Meetings'                        => 41,
            'DN_Scrum_Meetings'                     => 41,
            'DN_Scrum Meetings'                     => 41,
            'DN_Scrum_Meeting'                      => 41,
            'updates release planning'              => 41,
            '6S_Vacation (6-Systems)()'             => 58,
            self::DEFAULT_PROJECT_NAME              => 59,
            'DN_Grooming'                           => 69,
            'story telling'                         => 69,
            'Grooming'                              => 69,
            'DN_Continuous Improvement'             => 93,
            'DN_Continuous Improvement '            => 93,
            'DN_Continuous Improvement (Sixt)'      => 93,
            'Continuous Improvement (Sixt)'         => 93,
            'DN_Technical_Improvement'              => 138,
            'alignment with mobilers'               => 144,
            'DN_Fastlane'                           => 144,
            'DN_Fastlane (DriveNow)'                => 144,
            'DN_Lisbon (Sixt)'                      => 153,
            'DN_DriveNow/BMW Quartier (DriveNow)()' => 115,
            'DN_Split_SDP_Rating (Sixt)()'          => 190,
            "LAC_SMC_App"                           => 299,
            "LAC_SMC_App (Sixt Leasing)"            => 299,
            "LAC_SMC_App_1.0"                       => 284,
            "LAC_SMC_App_1.0 (Sixt Leasing)"        => 284,
            "LAC_SMC_2.0 (Sixt Leasing)"            => 246,
            "LAC_CarAbo (Sixt Leasing)"             => 303,
            "LAC_CarAbo"                            => 303
        ];
        $map_activity_name_to_activity_id = [
            'Project_Work'                 => 5,
            'Improvement Team Performance' => 58,
            'Improvement Knowledge Mgmt'   => 60,
            'Concept'                      => 238,
            'General Configuration'        => 145,
            self::DEFAULT_ACTIVITY_NAME    => 529,
            'Service Delivery Platform (SDP)' => 92,
            "Scrum and Project Management" => 704,
            "Scrum"                        => 817
        ];

        $kimaiProject = empty($kimaiProject) ? self::DEFAULT_PROJECT_NAME : $kimaiProject;
        $kimaiActivity = empty($kimaiActivity) ? self::DEFAULT_ACTIVITY_NAME : $kimaiActivity;
        $projectId = $map_project_name_to_project_id[$kimaiProject];
        $activityId = $map_activity_name_to_activity_id[$kimaiActivity];

        return [
            $kimaiProject  => empty($projectId) ? 59 : $projectId,
            $kimaiActivity => empty($activityId) ? 5 : $activityId
        ];
    }


    private function _setRequestHeaders($isLogin = false)
    {

        // Perform some version randomization, just in case
        $userAgentRandVersions =
            'Mozilla/5.0 '
            . '(X11; Linux x86_64) '
            . 'AppleWebKit/53%d.%d '
            . '(KHTML, like Gecko) '
            . 'Chrome/5%d.%d.%d.%d '
            . 'Safari/53%d.%d';

        $appleWebKit = [
            mt_rand(0, 9), mt_rand(10, 99),
        ];
        $userAgent = sprintf(
            $userAgentRandVersions,
            $appleWebKit[0], $appleWebKit[1],
            mt_rand(0, 9), mt_rand(0, 9), mt_rand(1000, 2785), mt_rand(100, 999),
            $appleWebKit[0], $appleWebKit[1]
        );
        $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36';

        $this->requestHeaders = [
            'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
            'User-Agent: ' . $userAgent,
            'X-Requested-With: XMLHttpRequest',
            'Origin: http://timetracking.sixt.de',
            'Accept: */*',
            'Referer: http://timetracking.sixt.de/kimai/core/kimai.php',
            'Connection: keep-alive'
        ];

        if ($isLogin) {
            $cookie = "Cookie: kimai_key=0; kimai_user=0";
            array_push($this->requestHeaders, $cookie);
        } else {
            $cookie = "Cookie: kimai_key=$this->kimaiKey; kimai_user=$this->username";
//            $cookieArr["cookie"] = $cookie;
//            $this->saveCookie($cookieArr);
            array_push($this->requestHeaders, $cookie);
        }
    }


    private function _setPostFields($params)
    {
        $this->postFields = $params;
    }

    private function saveCookie($cookie) {
        file_put_contents('kimaiCookie.json', json_encode($cookie, JSON_PRETTY_PRINT));
    }

}
