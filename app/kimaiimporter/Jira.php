<?php

class Jira
{

    private $username;
    private $password;

    private $apiUrl;
    private $apiUrlParamsTemplate;
    private $apiUrlParams;

    private $requestHeaders = [];
    private $postFields = [];


    /**
     * Jira constructor.
     *
     * @param array $params
     */
    function __construct($params)
    {
        $this->username = $params['username'];
        $this->password = $params['password'];

        $this->apiUrlParamsTemplate = $params['api.url.params'];
        $this->apiUrl         = $params['api.url'];
    }


    public function getUserIssuesByDateRange($params){

        $apiUrlParamsRendered = [
            'jql' => preg_replace(['/{{startDate}}/', '/{{endDate}}/'], $params, $this->apiUrlParamsTemplate['jql'])
        ];
        $this->apiUrlParams   = http_build_query($apiUrlParamsRendered);

        $url = $this->apiUrl . '?' . $this->apiUrlParams;
        // var_dump($url);
        $this->_setRequestHeaders();

        $result = $this->getData($url);

        return $result;
    }

    /**
     * Get data from Jira server
     *
     * @param string  $url
     *
     * @return mixed
     */
    protected function getData($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_HEADERFUNCTION, [$this, "checkAuthenticationStatus"]);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->requestHeaders);

        $result = curl_exec($ch);

        return $result;
    }


    private function checkAuthenticationStatus($curl, $headerLine)
    {
        preg_match('/X\-Seraph\-LoginReason:\s*AUTHENTICATED_FAILED/', $headerLine, $authentication_failed);
        preg_match('/X\-Seraph\-LoginReason:\s*AUTHENTICATION_DENIED/', $headerLine, $authentication_denied);
        preg_match('/X\-Seraph\-LoginReason:\s*AUTHORISATION_FAILED/', $headerLine, $authorisation_failed);

        if (!empty($authentication_failed[0])) {
            $this->authenticationDenied = true;
            echo shell_exec('echo "\033[0;37m\033[41mJira Authentication Failed. Check credentials and try again.\033[0m"');
            exit;
        }
        if (!empty($authentication_denied[0])) {
            $this->authenticationDenied = true;
            echo shell_exec('echo "\033[0;37m\033[41mJira Authentication Denied. Log into Jira from Web browser.\033[0m"');
            exit;
        }
        if (!empty($authorisation_failed[0])) {
            $this->authenticationDenied = true;
            echo shell_exec('echo "\033[0;37m\033[41mJira Authorisation failed. Check credentials and try again.\033[0m"');
            exit;
        }

        return strlen($headerLine);
    }


    private function getProjects()
    {

    }


    public function getProjectAndActivityIds($kimaiProject, $kimaiActivity)
    {
        $map_project_name_to_project_id = [
            "6s_vacation_(6-systems)" => 58,
            "lac_carabo_(sixt_leasing)" => 303,
            "lac_carabo" => 303,
            "dn_car2go_merge" => 268,
            "dn_car2go_merge_(drivenow)" => 268,
            "dn_strong_customer_authentication_" => 290,
            "dn_strong_customer_authentication_(sixt)" => 290,
            "dn_carinterface_adjustment_" => 286,
            "dn_carinterface_adjustment_(sixt)" => 286,
            "lac_smc_app_1.0_(sixt_leasing)" => 284,
            "lac_smc_app_1.0" => 284,
            "lac_smc_app" => 299,
            "lac_smc_app_(sixt_leasing)" => 299,
            "lac_smc_app_operations" => 302,
            "lac_smc_app_operations_(sixt_leasing)" => 302,
            "dn_b2b_product_(sixt)" => 158,
            "dn_b2b_tool_(drivenow)" => 224,
            "dn_b2b_tool" => 224,
            "dn_budapest" => 283,
            "dn_budapest_(sixt)" => 283,
            "dn_show_car2go_cars" => 260,
            "dn_rework_dn_partner_api" => 269,
            "dn_fastlaneii" => 233,
            "dn_trip_correction_(sixt)" => 191,
            "dn_security_measures" => 247,
            "dn_security_measures_(sixt)" => 247,
            "dn_small_orders" => 4,
            "dn_car_radar" => 234,
            "dn_async_reservation_(sixt)" => 134,
            "dn_bmw_rent_(sally)_(sixt)" => 114,
            "dn_bugfixing_maintenance_(sixt)" => 3,
            "dn_business_map_editor_(sixt)" => 15,
            "dn_carsharing_training_(6-systems)" => 33,
            "dn_configuration_application_(sixt)" => 135,
            "dn_continuous_improvement_(sixt)" => 93,
            "dn_copenhagen_support_(sixt)" => 133,
            "dn_customer_handshake_(drivenow)" => 121,
            "dn_drivenow_bmw_quartier_(drivenow)" => 115,
            "dn_drivenow_bmw_quartier_(base)" => 115,
            "dn_split_sdp_rating_(sixt)" => 190,
            "dn_durian_(drivenow)" => 116,
            "dn_fastlane_(drivenow)" => 144,
            "dn_grooming_(sixt)" => 69,
            "dn_legacy_desktop_website_(drivenow)" => 146,
            "dn_maas_(sixt)" => 143,
            "dn_milan_(sixt)" => 139,
            "dn_mobile_web_(drivenow)" => 107,
            "dn_mvp_website_blog relaunch_(drivenow)" => 147,
            "dn_nstack_devops_infrastructure_(sixt)" => 109,
            "dn_nstack_maintenance_support_(sixt)" => 126,
            "dn_one_way_sync_(sixt)" => 125,
            "dn_p_stack_reliability_(sixt)" => 142,
            "dn_reliability_(sixt)" => 142,
            "dn_paymentindependencesepa_(sixt)" => 129,
            "dn_postident_(sixt)" => 123,
            "dn_product_management_(sixt)" => 81,
            "dn_qlikview_(sixt)" => 113,
            "dn_reallocation_project_(drivenow)" => 118,
            "dn_registration_uk_(sixt)" => 122,
            "dn_regression_test_(sixt)" => 83,
            "dn_responsive_web_registration_(drivenow)" => 145,
            "dn_small_orders_(sixt)" => 4,
            "dn_bmw_small_orders_(sixt)" => 4,
            "dn_soap_api_(sixt)" => 92,
            "dn_support_tools_(sixt)" => 119,
            "dn_support(2nd)_(sixt)" => 23,
            "dn_support(3rd)_(sixt)" => 97,
            "dn_support_(sixt)" => 97,
            "dn_support(emergency)_(sixt)" => 101,
            "dn_sxindependence_(sixt)" => 106,
            "dn_technical_improvement_(sixt)" => 138,
            "dn_technikkreis_(sixt)" => 79,
            "dn_test_automation_(sixt)" => 85,
            "dn_training_conference_(6-systems)" => 100,
            "dn_web_maintenance_(6-systems)" => 149,
            "dn_web_smallorders_(drivenow)" => 110,
            "dn_website_blog_relaunch_(drivenow)" => 148,
            "dn_block_scrapers_(sixt)" => 155,
            '6s_others' => 59,
            'dn_lisbon_(sixt)' => 153,
        ];
        $map_activity_name_to_activity_id = [
            'project_work'                    => 5,
            Kimai::DEFAULT_ACTIVITY_NAME      => 529,
            'service_delivery_platform_(sdp)' => 92,
            "maintenance"                     => 719,
            "new-features"                    => 709,
            "payment"                         => 729,
            "lac_carabo_ordering"             => 731,
            "integration"                     => 732,
            // should never be used! why it's there, i wonder...
            "lac_carabo_project_work"         => 722,
            "lac_carabo_scrum"                => 817,
            "minor_improvements"              => 54,
        ];

        $kimaiProject = empty($kimaiProject) ? Kimai::DEFAULT_PROJECT_NAME : $kimaiProject;
        $kimaiActivity = empty($kimaiActivity) ? Kimai::DEFAULT_ACTIVITY_NAME  : $kimaiActivity;

        return [
            $kimaiProject  => empty($map_project_name_to_project_id[$kimaiProject]) ? 59
                : $map_project_name_to_project_id[$kimaiProject],
            $kimaiActivity => empty($map_activity_name_to_activity_id[$kimaiActivity]) ? 5
                : $map_activity_name_to_activity_id[$kimaiActivity],
        ];
    }


    public function processDayEvents($jiraIssue, $events, $config) {

        // Get array keys
        $arrayKeys = array_keys($events);
        // Fetch first array key
        $firstArrayKey = array_shift($arrayKeys);
        if(empty($firstArrayKey)){
            $firstArrayKey = 0;
        }
        // Fetch last array key
        $lastArrayKey = array_pop($arrayKeys);
        if(empty($lastArrayKey)){
            $lastArrayKey = 0;
        }


        $jiraEventTimes = [];
        $eventsCount = count($events);
        for($k = 0; $k < $eventsCount; ++$k){
            $event = [];
            $event1 = [];
            $additionalFields = [
                'description' => $jiraIssue['description'],
                'start_day' => $events[$k]['start_day'],
                'end_day' => $events[$k]['end_day'],
                'free' => 0,
                'kimai_project'  => $jiraIssue['projectId'],
                'kimai_activity' => $jiraIssue['activityId'],
            ];

            if($k == $firstArrayKey && $firstArrayKey != $lastArrayKey) {

                $start_time = $config["startOfDay"];
                $end_time   = $events[$k]['start_time'];

                $event += $this->addJiraTime($start_time, $end_time);
                $event += $additionalFields;

            } else if ($k != $firstArrayKey && $firstArrayKey != $lastArrayKey){

                $start_time = $events[$k-1]['end_time'];
                $end_time   = $events[$k]['start_time'];

                $jiraTime = $this->addJiraTime($start_time, $end_time);
                if(!empty($jiraTime)) {
                    $event1 += $jiraTime;
                    $event1 += $additionalFields;
                }
            } else if ($k == $firstArrayKey && $firstArrayKey == $lastArrayKey){

                $start_time = $config["startOfDay"];
                $end_time   = $events[$k]['start_time'];

                $event1 += $this->addJiraTime($start_time, $end_time);
                $event1 += $additionalFields;

            }

            if($k == $lastArrayKey) {

                $start_time = $events[$k]['end_time'];
                $end_time   = $config["endOfDay"];

                $event += $this->addJiraTime($start_time, $end_time);
                $event += $additionalFields;
            }

            if(!empty($event1))
                $jiraEventTimes[] = $event1;
            if(!empty($event))
                $jiraEventTimes[] = $event;
        }

        return $jiraEventTimes;
    }


    private function addJiraTime($start_time, $end_time){
        $arr = [];
        $startTime = new DateTime($start_time);
        $endTime = new DateTime($end_time);

        $duration = $startTime->diff($endTime)->format("%H:%I:%S");

        $timesEqual = ($startTime == $endTime);
        if(!$timesEqual){
            $arr = [
                'start_time' => $start_time,
                'end_time' => $end_time,
                'duration' => $duration,
            ];
        }

        return $arr;
    }


    private function _setRequestHeaders()
    {

        // Perform some version randomization, just in case
        $userAgentRandVersions =
            'Mozilla/5.0 '
            . '(X11; Linux x86_64) '
            . 'AppleWebKit/53%d.%d '
            . '(KHTML, like Gecko) '
            . 'Chrome/5%d.%d.%d.%d '
            . 'Safari/53%d.%d';

        $appleWebKit = [
            mt_rand(0, 9), mt_rand(10, 99),
        ];
        $userAgent = sprintf(
            $userAgentRandVersions,
            $appleWebKit[0], $appleWebKit[1],
            mt_rand(0, 9), mt_rand(0, 9), mt_rand(1000, 2785), mt_rand(100, 999),
            $appleWebKit[0], $appleWebKit[1]
        );
//        $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36';

        $this->requestHeaders = [
            'Authorization: Basic ' . base64_encode($this->username . ':' . $this->password),
            'Content-Type: application/json; charset=UTF-8',
            'User-Agent: ' . $userAgent,
            'X-Requested-With: XMLHttpRequest',
            'Accept: */*',
            'Connection: keep-alive'
        ];

    }

}
