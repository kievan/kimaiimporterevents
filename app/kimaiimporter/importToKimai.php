<?php
$kimaiUserName = (getenv('kimaiUserName')) ? : "default";
$configFilePath = __DIR__ . '/config.' . $kimaiUserName . '.php';
try {
    if(!file_exists($configFilePath)){
        throw new \Exception("$configFilePath does not exist, please create it by copying config.default.php.example");
    }
} catch(\Throwable $t) {
    echo PHP_EOL . $t->getMessage() . PHP_EOL;
    exit;
}
require_once($configFilePath);

require_once(__DIR__ . '/Kimai.php');
require_once(__DIR__ . '/Jira.php');

date_default_timezone_set($config['timezone']);

$commandLog = '';
$commandLog .= colorize_output('Hi,', $kimaiUserName, '38;5;112');

$shortopts = '';

$longopts  = array(
    "s:", // --s - startDayOffset (required value)
    "e:", // --e - endDayOffset (required value)
);
$options = getopt($shortopts, $longopts);
$startDayOffset = $config['startDayOffset'];
$endDayOffset   = $config['endDayOffset'];
if(!empty($options)){
    if(isset($options['s']) && is_numeric($options['s'])) {
        $startDayOffset = $options['s'] . ' day';
    } else if(isset($options['s']) && !is_numeric($options['s'])) {
        $startDayOffset = $options['s'];
    }
    if(isset($options['e']) && is_numeric($options['e'])){
        $endDayOffset = $options['e'] . ' day';
    } else if (isset($options['e']) && !is_numeric($options['e'])) {
        $endDayOffset = $options['e'];
    }

    if((int)$startDayOffset > (int)$endDayOffset){
        $commandLog .= sprintf("Start: %s, End: %s".PHP_EOL, (int)$startDayOffset, (int)$endDayOffset);
        $commandLog .= colorize_output('Start day offset MUST be smaller than end day offset. Exiting... ', '✗', '38;5;160');
        exit;
    }
}

//$office365 = new Office365($config['office365']['shared.calendar.url']);
$startDatetime =
    (new DateTime())
        ->modify($startDayOffset)
        ->format('Y-m-d\T00:01+03:00'); // format to the start of day
$endDatetime =
    (new DateTime())
        ->modify($endDayOffset)
        ->format('Y-m-d\T23:59+03:00'); // format to the end of day
$params = [
    'startDatetime' => $startDatetime,
    'endDatetime'   => $endDatetime,
];

$commandLog .= colorize_output('Working... ', '⇩', '38;5;226');

// Initilize events which will be imported into Kimai
$daysWithEvents = [];

try {
    $json = file_get_contents(__DIR__ . '/office365Events.json');
    $office365Events = json_decode($json, true);
    $office365Events = array_reverse($office365Events['value']);
} catch(\Throwable $t){
    $commandLog .= $t->getMessage();
    exit;
}

foreach ($office365Events as $event){

    $commandLog .= colorize_output('Processing Office 365 expanded event:', '.', '38;5;226');

    preg_match("/[Kk][Ii][Mm][Aa][Ii]:\s*([a-zA-Z0-9\s_\.\(\)]*)([\-\s\/\\\\]*)([a-zA-Z0-9\s]*)/", $event["Body"]["Content"], $kimaiProjectActivityMatch);

    // $date = (new DateTime())->format('c');
    // $commandLog .= PHP_EOL . $date . PHP_EOL;
    // $commandLog .= json_encode($event["Body"]["Content"], JSON_PRETTY_PRINT);
    // $commandLog .= PHP_EOL . $date . PHP_EOL;
    // $commandLog .= json_encode($kimaiProjectActivityMatch, JSON_PRETTY_PRINT);
    // $commandLog .= PHP_EOL . PHP_EOL;
    // klog($commandLog);


    if(empty($kimaiProjectActivityMatch)){
        $subject = $event["Subject"];
        $kimai_project = $config['kimai']['projectName'];
        $kimai_activity = $config['kimai']['eventActivityName'];
    } else {
        $kimai_project = empty($kimaiProjectActivityMatch[1]) ? $config['kimai']['projectName'] : trim($kimaiProjectActivityMatch[1]);
        $kimai_activity = empty($kimaiProjectActivityMatch[3]) ? $config['kimai']['eventActivityName'] : trim($kimaiProjectActivityMatch[3]);
    }

    $free = (int)($event['ShowAs'] !== 'Busy');
    $startDateTime = new Datetime($event['Start']['DateTime']);
    $endDateTime = new DateTime($event['End']['DateTime']);
    $startDateTime->setTimezone(new DateTimeZone($event["Start"]["TimeZone"]));
    $endDateTime->setTimezone(new DateTimeZone($event["End"]["TimeZone"]));
    $start_day = $startDateTime->format('d.m.Y');
    $end_day = $endDateTime->format('d.m.Y');
    $start_time = $startDateTime->format('H:i:s');
    $end_time = $endDateTime->format('H:i:s');

    $duration = $startDateTime->diff($endDateTime)->format("%H:%I:%S");
    $eventKey = $startDateTime->getTimestamp();
    if(0 === $free) {
        $daysWithEvents[$start_day][$eventKey] = [
            'description' => $event['Subject'],
            'start_day' => $start_day,
            'end_day' => $end_day,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'duration' => $duration,
            'free' => $free,
            'kimai_project' => $kimai_project,
            'kimai_activity' => $kimai_activity,
        ];
    }

}

/**
 * Add Kimai entries
 */
$kimai = new Kimai($config['kimai']);

foreach($daysWithEvents as $date => $o365Events) {
//    $date = (new DateTime())->setTimestamp($timestamp)->format("c");
    $commandLog .= PHP_EOL . "===== Date: $date =====" . PHP_EOL;

    /**
     * sort office 365 events for a specific day by timestamp
     */
    ksort($o365Events);
    $o365Events = array_values($o365Events);

    $jira = new Jira($config['jira']);
    $jiraStartDay = (new DateTime($o365Events[0]['start_day']))->modify('midnight')->modify('1 minute')->format('Y-m-d H:i');
    $jiraEndDay = (new DateTime($o365Events[0]['end_day']))->modify('tomorrow')->modify('1 minute ago')->format('Y-m-d H:i');
    $params = ['startDate' => $jiraStartDay, 'endDate' => $jiraEndDay, ];

    if(false === $config['kimai']['forceConfigDescription']) {
        $resultJson = $jira->getUserIssuesByDateRange($params);
        $result = json_decode($resultJson, true);

        $projectNameOrig = '.';
        // TODO: instead of relying on concrete field key, grep for projectName somehow
        if (!empty($result['issues'])) {
            $projectNameOrig = trim(strtolower(preg_replace('/[\ \/]/', '_', $result['issues'][0]['fields']['customfield_11752']['value'])));
            $projectName = (empty($projectNameOrig)) ? "6s_others" : $projectNameOrig;
        } else {
            $projectName = "6s_others";
        }
    } else {
        $projectName = trim(strtolower(preg_replace('/[\ \/]/', '_', $config['kimai']['projectName'])));
        $activityName = trim(strtolower(preg_replace('/[\ \/]/', '_', $config['kimai']['activityName'])));
    }

    // TODO: create activity name to id matching
    if(empty($activityName)) {
        $activityName = Kimai::DEFAULT_ACTIVITY_NAME;
    }
    $projectActivityIds = $jira->getProjectAndActivityIds($projectName, $activityName);
    $description =
        (!empty($result['issues'][0]['key']) ? $result['issues'][0]['key'] : $config['kimai']['jiraNumber'])
        . ' - '
        . (!empty($result['issues'][0]['fields']['summary'])  ? $result['issues'][0]['fields']['summary'] : $config['kimai']['jiraName'])
        . $config['kimai']['description']
        . (($projectActivityIds[$projectName] === 59) ? PHP_EOL . '  ' . $projectNameOrig : '');
    $jiraIssue = [
        'description' => $description,
        'projectId'   => $projectActivityIds[$projectName],
        'activityId'  => $projectActivityIds[$activityName],
    ];

    // TODO: subtract lunch time slots from tracked time
    $jiraEvents = $jira->processDayEvents($jiraIssue, $o365Events, $config);

//    $commandLog .= PHP_EOL . $date . PHP_EOL;
//    $commandLog .= json_encode($o365Events, JSON_PRETTY_PRINT);
//    $commandLog .= PHP_EOL . PHP_EOL;
//    $commandLog .= json_encode($jiraEvents, JSON_PRETTY_PRINT);
//    $commandLog .= PHP_EOL . $date . PHP_EOL . PHP_EOL;

    $commandLog .= addEventsToKimai($kimai, $o365Events);
    $commandLog .= addEventsToKimai($kimai, $jiraEvents);

    $commandLog .= "===== Date: $date =====" . PHP_EOL;
}


file_put_contents('logs/'.(new DateTime())->format('c') . '_command.log', $commandLog);


/**
 * @param Kimai $kimai
 * @param array $events
 * @return string json
 */
function addEventsToKimai($kimai, $events){

    $commandLog = '';
    foreach($events as $event) {
        $message = "({$event['start_time']} - {$event['end_time']}): {$event['description']}";
        $errors  = $kimai->addEntry($event);
        // $errors = null;
        if(empty($errors['errors'])) {
            $commandLog .= colorize_output('✓', $message,'38;5;112');
        } else {
            $output = colorize_output('✗', $message, '38;5;160');
            $errors['output'] = $output;
            $commandLog .= json_encode($errors, JSON_PRETTY_PRINT);
        }
    }

    return $commandLog;
}

//function colorize_output($message, $coloredText, $color){
//    return shell_exec("echo '\e[38;5;255m {$message} \e[39m \e[{$color}m $coloredText \e[39m'");
//}
function colorize_output($message, $coloredText, $color){
    return $message . " $coloredText" . PHP_EOL;
}

function append_output($message, $coloredText, $color){
    return shell_exec("echo '\e[38;5;255m {$message} \e[39m \e[{$color}m $coloredText \e[39m'");
}

function klog($commandLog) {
    file_put_contents('logs/'.(new DateTime())->format('c') . '_command.log', $commandLog);
}
